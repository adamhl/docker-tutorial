[[_TOC_]]

# Good Docker Practices

## Keep the Docker image simple (micro-services)

Although you can run as many processes in a single container as you want,
it is usually a good idea to design a container to do a single task. If
your application does several different things you can always add
"sidecar" containers that do the extra work.

There will be situations where splitting an application into different
containers is too complicated.  Be flexible and use your own judgement.

## Use small base images

A smaller image means faster start-up times and less memory used on the
container host. One way to acheive is to use a small base image. A popular
small image is `alpine` based on Alpine Linux. This is a complete Linux
with image size of 5.5MB with its own packages. By comparison,
`debian:buster-slim` is about 70MB.

On the other hand, don't let the drive toward small size get in the way
of needed functionality; remember the IBM Pollyanna Principle: "machines
should work; people should think".

## When possible use container orchestration

Getting containers to interact and cooperate can be tricky, so use one of
the orcestration tools like Kubernetes or Docker Compose to do this.

## Use CI/CD (i.e., automation) to keep Docker images up-to-date

Set up automation to rebuild your Docker images periodically making sure
that you disable caching when building. This way your image will have the
most up-to-date and secure base images.

## Send diagnostic output to standard output

In the traditional server world we are used to sending logs to files. With
Docker containers it is usually better to send diagnostic output to
standard output. Kubernetes and other orchestration tools are designed
with the expectation that logging is sent to standard output.

## Run containers in "read-only" mode

Running a Docker container in read-only mode helps to reduce the attack
surface area of your application. Mount external volumes for those parts
of the file system that need to be writable (`/var/log`, `/tmp`, etc.).
