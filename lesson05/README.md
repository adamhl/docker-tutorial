# Lesson 5: Network access

1. Change directory into `lesson05`.

1. A very popular use for containers is for serving web applications. How
do users get network access to a running container? Put simply, the host
acts as the network proxy for its containers: a network conection is made
to the host which directs the traffic into the container, and vice versa
for outbound traffic.

1. Let's try it:

        $ docker pull httpd
        $ docker run -dit --name apache --rm -p 8080:80 httpd
        $ docker ps
        CONTAINER ID   IMAGE     COMMAND              CREATED          STATUS          PORTS                  NAMES
        23c146b78377   httpd     "httpd-foreground"   20 seconds ago   Up 19 seconds   0.0.0.0:8080->80/tcp   apache

1. Note that traffic sent to port 8080 on the *host* gets sent to port 80
in the *container*. The `-d` option tells Docker to run the container in
"detached" mode, i.e., in the background.

1. Look at the network (look for the `Containers` element):

        $ docker network inspect bridge

1. Test the application. If running docker on your local machine put this
URL into your browser's address bar: `http://localhost:8080`.

1. If running on a remote host run this command:

        $ wget http://localhost:8080 --quiet -O -

1. Don't forget to kill the container:

        $ docker kill apache
