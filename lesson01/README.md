# Lesson 1: Docker basics and running a container

1. Download the `busybox` Docker image from Docker Hub:

        $ docker images
        $ docker pull busybox
        $ docker images

1. What do the columns mean? The first two are `REPOSITORY` and
`TAG`. Think of these as a way to name-space docker images. The
`REPOSITORY` is the name for a group of related repositories. For the case
of `busybox` the repository name is `busybox`. The second part of the
namespace is `TAG` and is separated from `REPOSITORY` with a `:`
(colon). If not explictly given, the tag defaults to `latest`.

1. We will discuss tagging and the other columns later.

1. Let's run busybox.

        $ docker run busybox /bin/sh -c "echo 'Hello' | md5sum"
        09f7e02f1290be211da707a266f153b3  -

1. What _is_ a docker container?

    > A container is a standard unit of software that packages up code and all
    > its dependencies so the application runs quickly and reliably from one
    > computing environment to another. (From https://www.docker.com)

1. At heart a Docker container is a set of processes running in a
["namespace"](https://en.wikipedia.org/wiki/Linux_namespaces). These
namespaces isolate the processes from the other processes running on the
server. You can think of all this as a light-weight virtual machine.

1. List the namespace of a running docker container (`lsns` is a Linux
command):

        $ docker run busybox /bin/sh -c "sleep 1000" &
        root> lsns  (must run as root to see the namespaces)

1. Because Docker containers are just processes running on an existing
server inside of a namespace, Docker images use the server's kernel. Thus,
only functionality supported by the underlying kernel will work in a
Docker container.

1. Docker containers also use ["control
groups"](https://en.wikipedia.org/wiki/Cgroups) which allow the host
operating system to put limits on the resources used by the running Docker
container. Limits can be placed on CPU, memory use, and I/O.

        # Limit docker to 10MB an use up all the memory
        # (idea from https://unix.stackexchange.com/questions/99334/how-to-fill-90-of-the-free-memory)
        $ docker run -m=10m busybox /bin/sh -c "cat /dev/zero | head -c 1m | tail"
        $ docker run -m=10m busybox /bin/sh -c "cat /dev/zero | head -c 20m | tail"

1. Unless you use an extra option the containers that you run will stick
around. To see this, use the `docker ps` command:

        $ docker ps --all
        $ docker ps -a    # (-a is the same as --all)

1. Note that the names of the containers are random words. To give your
container a name, use the `--name` command:

        $ docker run --name=fuzzle busybox /bin/sh -c "echo 'Hello' | md5sum"
        $ docker ps -a | grep fuzzle

1. To remove one of these left over containers use `docker rm`:

        $ docker ps -a | grep fuzzle
        $ docker rm fuzzle
        $ docker ps -a | grep fuzzle

1. To remove all stopped containers use `docker container prune`:

        $ docker ps -a
        $ docker container prune
        $ docker ps -a

1. To avoid the whole stopped container messiness, tell Docker to remove
the container once it exits with teh `--rm` option:

        $ docker run --rm --name=fuzzle busybox /bin/sh -c "echo 'Hello' | md5sum"
        $ docker ps -a | grep fuzzle

1. You can "login" to a running docker container:

        $ docker run --rm --name=fuzzle busybox /bin/sh -c "sleep 10000" &
        $ docker ps -a | grep fuzzle
        $ docker exec -ti fuzzle /bin/sh
        / # # You are "inside" the running container; run some commands
        / # ps -eaf
        / # df -h

1. The `-ti` options tell Docker that you want to allocate a pseudo-TTY
and use "interactive mode". *Warning:* logging into a running container is
not exactly like ssh'ing into a server: some commands that depend on the
terminal type may not work like you expect (e.g., editors, pagers, etc.)

1. Being able to login to a running container is **very** useful when debugging
your Docker builds.
