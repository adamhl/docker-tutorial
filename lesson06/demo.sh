#!/bin/bash
# demo.sh

if [ -z "${HELLO_WORLD}" ]; then
    echo "The environment variable HELLO_WORLD is not defined."
else
    echo "The environment variable HELLO_WORLD is '${HELLO_WORLD}'."
fi

