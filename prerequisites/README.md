# Prequisites

1. Installation of Docker.

1. Test your Docker install (the hex ID shown below will differ):

        $ docker images
        # REPOSITORY    TAG   IMAGE ID    CREATED         SIZE
        ... more ...

        $ docker pull busybox
        Using default tag: latest
        latest: Pulling from library/busybox
        e5d9363303dd: Pull complete

