# Summary of Docker commands used in this tutorial

    # List images
    docker images

    # Pull an image from Docker Hub
    docker pull busybox

    # Build a Docker image
    docker build -t my-tag .

    # Build image ignoring cached layers
    docker build --no-cache -t my-tag .

    # Run container in "interactive" mode
    docker run -it <image-name>

    # List containers
    docker ps
    docker ps -a  (include stopped containers)

    # Connect to a running container (i.e., "login")
    docker exec -it <container-name> -- /bin/bash
