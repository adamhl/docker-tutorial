# Lesson 6: Environment variables and configuration

1. Change directory into `lesson06`.

1. A popular way to provide configuration information to a docker container is
via environment variables.

1. Let's try it:

        $ docker build . -t demo
        $ docker run -it --rm --name fuzzle demo

1. Does the output make sense?

1. Let's pass in a value for the environment variable `HELLO_WORLD`:

        $ docker run -it --rm --name fuzzle -e HELLO_WORLD='Hello, world.' demo

1. You can also pass in environment variables via a file:

        $ cat file.env
        # We define two environment variables (you CAN comment!)
        HELLO_WORLD="Hello, world."
        ENV_VAR2="another environment variable"

        $ docker run -it --rm --name fuzzle --env-file=file.env demo

1. If your Docker application does not have many configuration options
configuring via environment variables is a good method. However, if the
application has complicated or extnesive configuration this may not be
feasible.

1. Don't forget to clean up:

        $ docker rmi demo:latest (removes the image demo:latest)
